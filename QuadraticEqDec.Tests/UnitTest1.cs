using System.Reflection;
using System;
using Xunit;

namespace QuadraticEqDec.Tests
{
    public class UnitTest1
    {
        const double eps = 1e-5;
        private bool EquationShouldHasRoots (double expected1, double expected2, double[] roots, double eps =1e-5)
        {
            return Math.Abs(roots[0]-expected1)<eps && Math.Abs(roots[1]-expected2)<eps;
        }

        [Fact]
        public void SquareOfTheDifferenceRoots ()
        {
            Class1 c =new Class1();

            double [] result = c.solve(1, -2, 1, eps);

            Assert.True(EquationShouldHasRoots(1,1,result, eps));
        }

        [Fact]
        public void IncompleteEquationRoots ()
        {
            Class1 c =new Class1();

            double [] result = c.solve(1, 0, -1, eps);

            Assert.True(EquationShouldHasRoots(1,-1,result, eps));
        }

        [Fact]
        public void NegativeDiscriminantTendsToZero ()
        {
            Class1 c =new Class1();

            double [] result = c.solve(1, 0, 1e-10, eps);

            Assert.True(EquationShouldHasRoots(0,0,result, eps));
        }

        [Fact]
        public void NoRootsWithNegativeDiscriminant ()
        {
            Class1 c =new Class1();

            double [] result = c.solve(1, 0, 1, eps);

            Assert.Empty(result);
        }

        [Fact]
        public void ACannotBeZero ()
        {
            Class1 c =new Class1();

            Assert.Throws<ArgumentException>(()=>c.solve(1e-10, 7, 1, eps));
        }
    }
}
