﻿using System;

namespace QuadraticEqDec
{
    public class Class1
    {
        public double[] solve (double a, double b, double c, double eps = 1e-5)
        {
            if (Math.Abs(a)<eps)
            {
                throw new ArgumentException("a connot be zero");
            }
            double d=b*b-4*a*c;
            if (d>=eps){
                double sqd=Math.Sqrt(d);
                return new double[2] {(sqd-b)/2/a, (-sqd-b)/2/a};
            } else {
                if (Math.Abs(d)<eps){
                    return new double[2] {-b/2/a, -b/2/a};
                } else {
                    return new double[0];
                }
            }
        }
    }
}
